#pragma once

#include <DirectXMath.h>
#include "Mesh.h"

using namespace DirectX;

class GameEntity
{
	bool isDirty;

	XMFLOAT4X4 world;

	XMFLOAT3 position;
	XMFLOAT4 rotation;
	XMFLOAT3 scale;

	void RecalculateWorld();

public:
	GameEntity();
	GameEntity(Mesh* mesh);
	~GameEntity();

	bool IsDirty();
};

