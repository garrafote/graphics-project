#pragma once
#include <d3d11.h>
#include "Vertex.h"
#include "macros.h"

class Mesh
{
	ID3D11Buffer* vertexBuffer;
	ID3D11Buffer* indexBuffer;

	int indexCount;
	ID3D11Device* device;

public:	
	Mesh(ID3D11Device* device, Vertex* vertices, int vertexCount, unsigned int* indices, int indexCount);
	Mesh();
	~Mesh();

	ID3D11Buffer* const* GetVertexBuffer();
	ID3D11Buffer* GetIndexBuffer();
	int GetIndexCount();
};

