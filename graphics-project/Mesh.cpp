#include "Mesh.h"


Mesh::Mesh(ID3D11Device* device, Vertex* vertices, int vertexCount, UINT* indices, int indexCount) : indexCount(indexCount), device(device)
{
	// --- Vertex ---

	D3D11_BUFFER_DESC vertexBufferDesc;
	vertexBufferDesc.ByteWidth				= sizeof(Vertex) * vertexCount;
	vertexBufferDesc.Usage					= D3D11_USAGE_IMMUTABLE;
	vertexBufferDesc.BindFlags				= D3D11_BIND_VERTEX_BUFFER;
	vertexBufferDesc.CPUAccessFlags			= 0; //D3D11_CPU_ACCESS_READ | D3D11_CPU_ACCESS_WRITE;
	vertexBufferDesc.MiscFlags				= 0;
	vertexBufferDesc.StructureByteStride	= 0;

	D3D11_SUBRESOURCE_DATA initialVertexData;
	initialVertexData.pSysMem = vertices;

	HR(device->CreateBuffer(&vertexBufferDesc, &initialVertexData, &vertexBuffer));


	// --- Index ---

	D3D11_BUFFER_DESC indexBufferDesc;
	indexBufferDesc.ByteWidth				= sizeof(UINT) * indexCount;
	indexBufferDesc.Usage					= D3D11_USAGE_IMMUTABLE;
	indexBufferDesc.BindFlags				= D3D11_BIND_INDEX_BUFFER;
	indexBufferDesc.CPUAccessFlags			= 0; //D3D11_CPU_ACCESS_READ | D3D11_CPU_ACCESS_WRITE;
	indexBufferDesc.MiscFlags				= 0;
	indexBufferDesc.StructureByteStride		= 0;	
	
	D3D11_SUBRESOURCE_DATA initialIndexData;
	initialIndexData.pSysMem = indices;

	HR(device->CreateBuffer(&indexBufferDesc, &initialIndexData, &indexBuffer));
}

Mesh::Mesh() : indexCount(0)
{
}

Mesh::~Mesh()
{
	ReleaseMacro(vertexBuffer);
	ReleaseMacro(indexBuffer);
}


ID3D11Buffer* const* Mesh::GetVertexBuffer()
{
	return &vertexBuffer;
}

ID3D11Buffer* Mesh::GetIndexBuffer()
{
	return indexBuffer;
}

int Mesh::GetIndexCount()
{
	return indexCount;
}
